#!/bin/zsh
TEST_SCRIPT="/home/cberst/code/python/css-tests/abortCancelRunDAT.py"
DLU_NAME="CSS"
DAT_LIB_PATH="/home/cberst/css/dat-library"
INST="vbi"
LIB_PATH=${DAT_LIB_PATH}/${INST}

# We need the helper functions, specifically 'seconds2dhmsString()'
source ${DAT_LIB_PATH}/util/helper.functions

TYPE=Interp
SHELL=jython
AJAVA=${ATST}/bin/ajava
CLASS=atst.cs.util.scripting.ScriptReader\$${TYPE}
set -A SCRIPT_READER_ARGS
SCRIPT_READER_ARGS+="--shell=jython"
SCRIPT_READER_ARGS+="--noprompt"
SCRIPT_READER_ARGS+="--file=${TEST_SCRIPT}"
SCRIPT_READER_ARGS+="--"

DATS_TO_RUN=""

if [[ ${DATS_TO_RUN} == "oneShot" ]]; then
  DAT_FILE=${LIB_PATH}/"SOME DATFILE NAME"
  CMD_LINE_OPTIONS=(\
    # Max time to let dat run for abort/cancel tests
    "--datDuration 30 "\

    # xyz = absolute/timeSlice
    "--triggerReference xyz "\

    # xyz = [preview | cancel | abort | noAction | mixItUp]
    # Multiple test modes may be included and/or repeated.
    # If '--modesToTest' is omitted then All modes will be tested.
    "--modesToTest xyz " \

    # Number of times to execute each modesToTest, value >= 1
    "--numPasses x "\

    # Number of times to execute the DAT in each pass, value >= 1
    "--execCount x "\

    "--previewCcDefault "\
    "--fastAsPossible "\
    "--deleteAllIds ")

  RunScript --exec --noprompt --file=${TEST_SCRIPT} -- ${DLU_NAME} --datFile ${DAT_FILE} ${CMD_LINE_OPTIONS}

else;
  # Use this section for running multiple DATs

  #startModes=(immediate)
  startModes=(atStartTime)
  #startModes=(immediate atStartTime)

  #triggerReferenceModes=(absolute)
  triggerReferenceModes=(timeSlice)
  #triggerReferenceModes=(absolute timeSlice)

  EXEC_COUNT=1              # Number of times to execute the DAT in each pass
  NUM_PASSES=1              # Number of times to execute each "modesToTest"

  # All modes: (preview abort cancel noAction mixItUp)
  # Multiple test modes may be included and/or repeated.
  modes_to_test=(preview abort cancel)

  #
  # "Long" running DATs: DATs that have a single pass duration of >10min
  #
  longRunningDATS=(
  "stdUseCases/Center-Field-Single-Filter-30Hz.dat"\                 # DAT Duration = 2666.666667 sec  == 0:44:26.666667 (single pass)
  "stdUseCases/All-Fields-Single-Filter-30Hz.dat"                    # DAT Duration = 27420.000000 sec == 7:37:00.000000 (single pass)
  )

  #
  # "Short" running DATs: DATs that have a single pass duration <=10min
  #
  shortRunningDATs=(
  "collected/be2e_Test_01.dat"
  "collected/HLSOPS-555_vbi_4filter_9field-mixed200-80frames_30Hz_mixed.dat"\   # DAT Duration = 217.680000 sec == 0:03:37.680000
  "stdUseCases/All-Fields-All-Filter-30Hz.dat"\                      # DAT Duration = 557.400000 sec   == 0:09:17.400000 (single pass)
  "stdUseCases/Focus.dat"\                                           # DAT Duration = 86.583333 sec    == 0:01:26.583333 (single pass)
  "stdUseCases/Focus-Full-Range.dat"\                                # DAT Duration = 86.583333 sec    == 0:01:26.583333 (single pass)
  "stdUseCases/Mixed-Filter-Field-Exposure-w-Grouping.dat"           # DAT Duration = 305.676667 sec   == 0:05:05.676667 (single pass)
  )

  set -A listOfDatFiles ${longRunningDATS}
  listOfDatFiles+=(${shortRunningDATs})

  total_exec=0              # Total number of .dat files executed
  total_pass=0              # Total number of executions that passed
  total_fail=0              # Total number of executions that failed

  set -A dat_file_status    # to collect Pass/Fail and filename

  startDate=$(date)         # Save starting date/time

  for fileName in ${listOfDatFiles}; do
    for triggerReference in ${triggerReferenceModes}; do
      for startMode in ${startModes}; do
        set -A all_args ${SCRIPT_READER_ARGS}
        all_args+=(${DLU_NAME})
        all_args+=("--datFile")
        all_args+=(${LIB_PATH}/${fileName})
        all_args+=("--modesToTest")
        for mode in ${modes_to_test}; do
          all_args+=(${mode})
        done
        all_args+=("--datDuration")
        all_args+=("1.5")
        all_args+=("--triggerReference")
        all_args+=(${triggerReference})
        all_args+=("--startMode")
        all_args+=(${startMode})
        all_args+=("--numPasses")
        all_args+=(${NUM_PASSES})
        all_args+=("--execCount")
        all_args+=(${EXEC_COUNT})
        all_args+=("--previewCcDefault")
        all_args+=("--deleteAllIds")
        all_args+=("--fastAsPossible")

        echo "================================================================================"
        echo "Executing: ${INST}/${fileName}"
        echo

        ${AJAVA} ${CLASS} ${all_args}
        exit_code=$?

        echo
        echo

        total_exec=$((total_exec+1))
        if [[ ${exit_code} -eq 0 ]]; then
          dat_file_status+="PASS: ${INST}/${fileName} ${triggerReference} ${startMode} (${modes_to_test})"
          total_pass=$((total_pass+1))
        else;
          dat_file_status+="FAIL: ${INST}/${fileName} ${triggerReference} ${startMode} (${modes_to_test})"
          total_fail=$((total_fail+1))
        fi

      done
    done
  done

  endDate=$(date)           # Save ending date/time
fi

if [[ ${DATS_TO_RUN} != "oneShot" ]]; then
  echo "================================================================================"
  echo "=                                 TEST SUMMARY                                 ="
  echo "================================================================================"
  echo "Started @: ${startDate}"
  echo "  Ended @: ${endDate}"

  # Use end-start to calculate a duration in seconds and then convert to d/h/m/s string
  ((duration=$(date -d $endDate +%s)-$(date -d $startDate +%s)))
  echo " Duration:" `seconds2dhmsString ${duration}`

  echo
  echo "Overall test file status: "
  for name in ${dat_file_status}; do
    echo "\t${name}"
  done
  echo
  echo "Total executed: ${total_exec}"
  echo "Total passed  : ${total_pass}"
  echo "Total failed  : ${total_fail}"
  echo
  if [[ ${total_fail} -eq 0 ]] then
    echo "Overall Status: SUCCESS"
  else
    echo "Overall Status: FAILURE"
  fi
fi
