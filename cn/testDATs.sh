#!/bin/zsh
TEST_SCRIPT="/home/cberst/code/python/css-tests/abortCancelRunDAT.py"
DLU_NAME="CSS"
DAT_LIB_PATH="/home/cberst/css/dat-library"
INST="cn"
LIB_PATH=${DAT_LIB_PATH}/${INST}

# We need the helper functions, specifically 'seconds2dhmsString()'
source ${DAT_LIB_PATH}/util/helper.functions

TYPE=Interp
SHELL=jython
AJAVA=${ATST}/bin/ajava
CLASS=atst.cs.util.scripting.ScriptReader\$${TYPE}
set -A SCRIPT_READER_ARGS
SCRIPT_READER_ARGS+="--shell=jython"
SCRIPT_READER_ARGS+="--noprompt"
SCRIPT_READER_ARGS+="--file=${TEST_SCRIPT}"
SCRIPT_READER_ARGS+="--"

DATS_TO_RUN=""

if [[ ${DATS_TO_RUN} == "oneShot" ]]; then
  DAT_FILE=${LIB_PATH}/"SOME DATFILE NAME"
  CMD_LINE_OPTIONS=(\
    # Max time to let dat run for abort/cancel tests
    "--datDuration 30 "\

    # xyz = absolute/timeSlice
    "--triggerReference xyz "\

    # xyz = [preview | cancel | abort | noAction | mixItUp]
    # Multiple test modes may be included and/or repeated.
    # If '--modesToTest' is omitted then All modes will be tested.
    "--modesToTest xyz " \

    # Number of times to execute each modesToTest, value >= 1
    "--numPasses x "\

    # Number of times to execute the DAT in each pass, value >= 1
    "--execCount x "\

    "--previewCcDefault "\
    "--fastAsPossible "\
    "--deleteAllIds ")

  RunScript --exec --noprompt --file=${TEST_SCRIPT} -- ${DLU_NAME} --datFile ${DAT_FILE} ${CMD_LINE_OPTIONS}

else;
  # Use this section for running multiple DATs

  startModes=(immediate)
  startModes=(atStartTime)
  startModes=(immediate atStartTime)

  triggerReferenceModes=(absolute)
  triggerReferenceModes=(timeSlice)
  triggerReferenceModes=(absolute timeSlice)

  EXEC_COUNT=1              # Number of times to execute the DAT in each pass
  NUM_PASSES=1              # Number of times to execute each "modesToTest"

  # All modes: (preview abort cancel noAction mixItUp)
  # Multiple test modes may be included and/or repeated.
  modes_to_test=(preview abort cancel noAction)

  #
  # "Long" running DATs: DATs that have a single pass duration of >10min
  #
  longRunningDATS=(
    "collected/HLSOPS-407_NotFinishedOnTime.dat"                     # DAT Duration = 4hr
  )

  #
  # "Short" running DATs: DATs that have a single pass duration <=10min
  # FUTR == FastUpTheRamp
  # SUTR == SlowUpTheRamp
  # LBL  == LineByLine
  #
  shortRunningDATs=(
    "contrived/FastAsPossible_FUTR_Continuous_3ndr_x200.dat"
    "contrived/FastAsPossible_FUTR_Stepped_3ndr_x200.dat"
    "contrived/FastAsPossible_SUTR_Stepped_2ndr_x50.dat"
    "contrived/FastAsPossible_LBL_Stepped_1ndr_x200.dat"
  )

  set -A listOfDatFiles ${longRunningDATS}
  listOfDatFiles+=(${shortRunningDATs})

  total_exec=0              # Total number of .dat files executed
  total_pass=0              # Total number of executions that passed
  total_fail=0              # Total number of executions that failed

  set -A dat_file_status    # to collect Pass/Fail and filename

  startDate=$(date)         # Save starting date/time

  for fileName in ${listOfDatFiles}; do
    for triggerReference in ${triggerReferenceModes}; do
      for startMode in ${startModes}; do
        set -A all_args ${SCRIPT_READER_ARGS}
        all_args+=(${DLU_NAME})
        all_args+=("--datFile")
        all_args+=(${LIB_PATH}/${fileName})
        all_args+=("--modesToTest")
        for mode in ${modes_to_test}; do
          all_args+=(${mode})
        done
        all_args+=("--datDuration")
        all_args+=("1.5")
        all_args+=("--triggerReference")
        all_args+=(${triggerReference})
        all_args+=("--startMode")
        all_args+=(${startMode})
        all_args+=("--numPasses")
        all_args+=(${NUM_PASSES})
        all_args+=("--execCount")
        all_args+=(${EXEC_COUNT})
        all_args+=("--previewCcDefault")
        all_args+=("--deleteAllIds")
        all_args+=("--fastAsPossible")

        echo "================================================================================"
        echo "Executing: ${INST}/${fileName}"
        echo

        ${AJAVA} ${CLASS} ${all_args}
        exit_code=$?

        echo
        echo

        total_exec=$((total_exec+1))
        if [[ ${exit_code} -eq 0 ]]; then
          dat_file_status+="PASS: ${INST}/${fileName} ${triggerReference} ${startMode} (${modes_to_test})"
          total_pass=$((total_pass+1))
        else;
          dat_file_status+="FAIL: ${INST}/${fileName} ${triggerReference} ${startMode} (${modes_to_test})"
          total_fail=$((total_fail+1))
        fi

      done
    done
  done

  endDate=$(date)           # Save ending date/time
fi

if [[ ${DATS_TO_RUN} != "oneShot" ]]; then
  echo "================================================================================"
  echo "=                                 TEST SUMMARY                                 ="
  echo "================================================================================"
  echo "Started @: ${startDate}"
  echo "  Ended @: ${endDate}"

  # Use end-start to calculate a duration in seconds and then convert to d/h/m/s string
  ((duration=$(date -d $endDate +%s)-$(date -d $startDate +%s)))
  echo " Duration:" `seconds2dhmsString ${duration}`

  echo
  echo "Overall test file status: "
  for name in ${dat_file_status}; do
    echo "\t${name}"
  done
  echo
  echo "Total executed: ${total_exec}"
  echo "Total passed  : ${total_pass}"
  echo "Total failed  : ${total_fail}"
  echo
  if [[ ${total_fail} -eq 0 ]] then
    echo "Overall Status: SUCCESS"
  else
    echo "Overall Status: FAILURE"
  fi
fi
