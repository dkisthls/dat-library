#==========================================================
#        Date: 2023-01-30
# FileContent: SubmitDirect Sequence
#        JIRA: HLSOPS-2692 Sim Only: NPE when setting an 
#              exposure time following a change in shutter mode
# Camera/Inst: Andor Zyla
# CSS Version: trunk
#     Comment: The complete series of submits for an Andor
#              camera that leads to an NPE being caused by
#              an attempted restart of a DAT that has been
#              deleted due to a change in shutter mode.
#              Error only occurred using the simulator but
#              the resolution, detailed in HLSOPS-2692, 
#              required changes to both simulator and real
#              camera code.
#
# Submit Using: ajava atst.css.tools.CssCcEbToosl\$SubmitDirect
# Sequence:
#  1) Set Global Shutter
#  2) Submit cc_1 with Min ExpTime/Max ExpRate for global
#     shutter, full frame 
#  3) Run cc_1 500 times
#  4) Change to rolling shutter
#  5) Set ExpTime in cc_1 to min for rolling shutter
#  6) Run cc_1 500 times
#  7) 
# 
#
#==========================================================

# csh - Assure we start with Global Shutter
{"table":{"css.tools.csh:shutter:mode":["Global"]}}

#############################################################################

# cc_1, Min Exp Time/Max Exp rate for full frame, global shutter
{"table":{"css.tools.camConfig:ID":["cc_1"],"css.tools.camConfig:accum:numCycles":["1"],"css.tools.camConfig:accum:numberOf":["1"],"css.tools.camConfig:accum:posAtRefT0":["1"],"css.tools.camConfig:accum:sequence":["1"],"css.tools.camConfig:bufferEnable":["false"],"css.tools.camConfig:description":["Default camera Configuration."],"css.tools.camConfig:exposure:rate":["49.53396215884354"],"css.tools.camConfig:exposure:rateMultiplier":["1"],"css.tools.camConfig:exposure:ratePhase":["0.0"],"css.tools.camConfig:exposure:time":["0.0092395"],"css.tools.camConfig:normalization:method":["none"],"css.tools.camConfig:normalization:value":["0.0"],"css.tools.camConfig:winBin:hw:binSize":["1","1"],"css.tools.camConfig:winBin:hw:win:ROI_1":["0","0","2560","2160"],"css.tools.camConfig:winBin:hw:win:ROI_2":["0","0","1","1"],"css.tools.camConfig:winBin:hw:win:ROI_3":["0","0","1","1"],"css.tools.camConfig:winBin:hw:win:ROI_4":["0","0","1","1"],"css.tools.camConfig:winBin:hw:win:numberOfROIs":["1"],"css.tools.camConfig:winBin:sw:binSize":["1","1"],"css.tools.camConfig:winBin:sw:win:ROI_1":["0","0","2560","2160"],"css.tools.camConfig:winBin:sw:win:ROI_2":["0","0","1","1"],"css.tools.camConfig:winBin:sw:win:ROI_3":["0","0","1","1"],"css.tools.camConfig:winBin:sw:win:ROI_4":["0","0","1","1"],"css.tools.camConfig:winBin:sw:win:numberOfROIs":["1"],"css.tools.camConfig:winBin:sw:win:roiType":["vertical"]}}

#############################################################################

# globals - Start cc_1 to run 500 times
{"table":{"css.tools.global:camMode":["startWithoutSave"],"css.tools.global:execCount0":["500"],"css.tools.global:ID0":["cc_1"],"css.tools.global:startMode":["immediate"],"css.tools.global:triggerReference":["absolute"]}}

#############################################################################

# csh - Change to Rolling shutter
{"table":{"css.tools.csh:shutter:mode":["Rolling"]}}

#############################################################################

# The CSS was running Global shutter, executing cc_1 with an exposure time of 
# 0.0092395ms. Following the change to Rolling shutter, the cc_1 exposure time 
# is no longer valid. It is < Rolling shutter minimum exposure time of 
# 0.0277184ms. The CSS will automatically start cc_default since cc_1 can no 
# longer run.

# We now want to change the exposure time of cc_1 to be a valid Rolling shutter exposure time.
# cc_1
{"table":{"css.tools.camConfig:ID":["cc_1"],"css.tools.camConfig:accum:numCycles":["1"],"css.tools.camConfig:accum:numberOf":["1"],"css.tools.camConfig:accum:posAtRefT0":["1"],"css.tools.camConfig:accum:sequence":["1"],"css.tools.camConfig:bufferEnable":["false"],"css.tools.camConfig:description":["Default camera Configuration."],"css.tools.camConfig:exposure:rate":["49.53396215884354"],"css.tools.camConfig:exposure:rateMultiplier":["1"],"css.tools.camConfig:exposure:ratePhase":["0.0"],"css.tools.camConfig:exposure:time":["0.0277184"],"css.tools.camConfig:normalization:method":["none"],"css.tools.camConfig:normalization:value":["0.0"],"css.tools.camConfig:winBin:hw:binSize":["1","1"],"css.tools.camConfig:winBin:hw:win:ROI_1":["0","0","2560","2160"],"css.tools.camConfig:winBin:hw:win:ROI_2":["0","0","1","1"],"css.tools.camConfig:winBin:hw:win:ROI_3":["0","0","1","1"],"css.tools.camConfig:winBin:hw:win:ROI_4":["0","0","1","1"],"css.tools.camConfig:winBin:hw:win:numberOfROIs":["1"],"css.tools.camConfig:winBin:sw:binSize":["1","1"],"css.tools.camConfig:winBin:sw:win:ROI_1":["0","0","2560","2160"],"css.tools.camConfig:winBin:sw:win:ROI_2":["0","0","1","1"],"css.tools.camConfig:winBin:sw:win:ROI_3":["0","0","1","1"],"css.tools.camConfig:winBin:sw:win:ROI_4":["0","0","1","1"],"css.tools.camConfig:winBin:sw:win:numberOfROIs":["1"],"css.tools.camConfig:winBin:sw:win:roiType":["vertical"]}}

#############################################################################
#
# NPE OCCURED HERE
#
#############################################################################

# cc_1 now has a valid Rolling shutter exposure time. 
# Run it again with Rolling shutter.
# globals
{"table":{"css.tools.global:camMode":["startWithoutSave"],"css.tools.global:execCount0":["500"],"css.tools.global:ID0":["cc_1"],"css.tools.global:startMode":["immediate"],"css.tools.global:triggerReference":["absolute"]}}

#############################################################################

# cc_default, 1Hz, 20ms exposure time
{"table":{"css.tools.camConfig:ID":["cc_default"],"css.tools.camConfig:accum:numCycles":["1"],"css.tools.camConfig:accum:numberOf":["1"],"css.tools.camConfig:accum:posAtRefT0":["1"],"css.tools.camConfig:accum:sequence":["1"],"css.tools.camConfig:bufferEnable":["false"],"css.tools.camConfig:description":["Default camera Configuration."],"css.tools.camConfig:exposure:rate":["1.0"],"css.tools.camConfig:exposure:rateMultiplier":["1"],"css.tools.camConfig:exposure:ratePhase":["0.0"],"css.tools.camConfig:exposure:time":["20.0"],"css.tools.camConfig:normalization:method":["none"],"css.tools.camConfig:normalization:value":["0.0"],"css.tools.camConfig:winBin:hw:binSize":["1","1"],"css.tools.camConfig:winBin:hw:win:ROI_1":["0","0","2560","2160"],"css.tools.camConfig:winBin:hw:win:ROI_2":["0","0","1","1"],"css.tools.camConfig:winBin:hw:win:ROI_3":["0","0","1","1"],"css.tools.camConfig:winBin:hw:win:ROI_4":["0","0","1","1"],"css.tools.camConfig:winBin:hw:win:numberOfROIs":["1"],"css.tools.camConfig:winBin:sw:binSize":["1","1"],"css.tools.camConfig:winBin:sw:win:ROI_1":["0","0","2560","2160"],"css.tools.camConfig:winBin:sw:win:ROI_2":["0","0","1","1"],"css.tools.camConfig:winBin:sw:win:ROI_3":["0","0","1","1"],"css.tools.camConfig:winBin:sw:win:ROI_4":["0","0","1","1"],"css.tools.camConfig:winBin:sw:win:numberOfROIs":["1"],"css.tools.camConfig:winBin:sw:win:roiType":["vertical"]}}

#############################################################################

# csh - Back to Global Shutter
{"table":{"css.tools.csh:shutter:mode":["Global"]}}

#############################################################################

# Restart cc_default
# globals
{"table":{"css.tools.global:camMode":["preview"],"css.tools.global:execCount0":["0"],"css.tools.global:ID0":["cc_default"],"css.tools.global:startMode":["immediate"],"css.tools.global:triggerReference":["absolute"]}}

# NOTE: 
# A this point CSS should be idling cc_default @ 1Hz, 20ms exposure time.
# If this is the case then the code is working properly.

#############################################################################

