#!/bin/zsh
source ../util/helper.functions

numFailures=0
VCNAME=cb.atst.vcctt
basePath=/home/cberst/DKISTDropbox/myDevel/DL-NIRSP/debug/OCP1.2

#ajava atst.css.tools.CssCcEbTools$\SubmitDirect --vcName=${VCNAME} --fileName=/home/cberst/DKISTDropbox/myDevel/DL-NIRSP/debug/OCP1.2/HLSOPS-1079_NpeOnRestartOfNonExistantDat.sds --help
doSubmitDirect ${VCNAME} "HLSOPS-1079_NpeOnRestartOfNonExistentDat.sds"
testResult=$?

testStatus="OK"
if [[ ${testResult} -ne 0 ]]; then
  testStatus="FAILURE"
  numFailures+=1
fi

echo "HLSOPS-1079_NpeOnRestartOfNonExistentDat.sds...${testStatus}"

overallStatus="OK"
if [[ ${numFailures} -ne 0 ]]; then
  overallStatus="FAILURE"
fi
echo
echo "Overall status...${overallStatus}"
echo

