#!/bin/zsh
source ../util/helper.functions

numFailures=0
VCNAME=woody.atst.vcctt

cssUp

doSubmitDirect ${VCNAME} "HLSOPS-1079.sds"
testResult=$?

testStatus="OK"
if [[ ${testResult} -ne 0 ]]; then
  testStatus="FAILURE"
  numFailures+=1
fi

echo "HLSOPS-1079.sds...${testStatus}"

overallStatus="OK"
if [[ ${numFailures} -ne 0 ]]; then
  overallStatus="FAILURE"
fi
echo
echo "Overall status...${overallStatus}"
echo

cssDown

